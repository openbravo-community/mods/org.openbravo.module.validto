/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2010-2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.validto.process;

import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.AcctProcessTemplate;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocLine;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.ad_forms.FactLine;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.service.db.DalConnectionProvider;

/**
 * Accounting process that checks the
 * <ul>
 * <li>Business Partner</li>
 * <li>Tax Rate</li>
 * <li>Element Value (Account)</li>
 * </ul>
 * have a valid to field >= Document's Accounting date
 * 
 * @author openbravo
 * 
 */
public class ValidToProcess implements AcctProcessTemplate {
  static Logger log4j = Logger.getLogger(ValidToProcess.class);

  final ValidToProcessDao dao = new ValidToProcessDao();

  @Override
  public boolean execute(final AcctServer acctServer, final AcctSchema as,
      final ConnectionProvider conn, final Connection con, final VariablesSecureApp vars)
      throws ServletException {

    /*
     * Reading Data
     */
    final String varsLang = vars.getLanguage();
    final String language = (varsLang == null || varsLang.equals("")) ? "en_US" : varsLang;

    final String documentNo = acctServer.DocumentNo;
    final String docType = acctServer.DocumentType;
    log4j.debug("Checking valid to dates for document: " + documentNo + " (" + docType + ")...");

    Date acctDate;
    try {
      final String dateFormat = OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty("dateFormat.java");
      acctDate = new SimpleDateFormat(dateFormat).parse(acctServer.DateAcct);
      log4j.debug("Accounting Date: " + acctDate);
    } catch (final ParseException e) {
      log4j.error("Error Parsing the date: " + acctServer.DateAcct + " using the JavaDateFormat: "
          + vars.getJavaDateFormat(), e);
      return false;
    }

    // Checking the Business Partner Valid To field (Header level)
    BusinessPartner businessPartner = dao.getBusinessPartner(acctServer.C_BPartner_ID);
    if (businessPartner == null) {
      log4j.debug("No Business Partner detected at header level");
    } else {
      log4j.debug("Business Partner: " + businessPartner.getIdentifier());
      if (!ValidToUtility.hasValidDateTo(businessPartner, acctDate)) {
        log4j.error("The Business Partner " + businessPartner.getIdentifier()
            + " has a valid to date less than " + acctDate);
        setError(businessPartner, acctServer, language);
        return false;
      }
    }

    for (final Fact fact : acctServer.m_fact) {
      for (final FactLine line : fact.getLines()) {
        if (new BigDecimal(line.getM_AmtAcctCr()).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(line.getM_AmtAcctDr()).compareTo(BigDecimal.ZERO) != 0) {

          // Checking the Account Valid To field
          final ElementValue account = dao.getElementValue(line.getM_acct().getAccount_ID());
          if (account == null) {
            log4j.error("No Account detected");
          } else {
            log4j.debug("Acount: " + account.getIdentifier());
            if (!ValidToUtility.hasValidDateTo(account, acctDate)) {
              log4j.error("The Account " + account.getIdentifier()
                  + " has a valid to date less than " + acctDate);
              setError(account, acctServer, language);
              return false;
            }
          }

          final DocLine docLine = line.getM_docLine();
          if (docLine != null) {
            // Checking the Business Partner Valid To field (Line level)
            businessPartner = dao.getBusinessPartner(docLine.m_C_BPartner_ID);
            if (businessPartner != null) {
              log4j.debug("Business Partner: " + businessPartner.getIdentifier());
              if (!ValidToUtility.hasValidDateTo(businessPartner, acctDate)) {
                log4j.error("The Business Partner " + businessPartner.getIdentifier()
                    + " has a valid to date less than " + acctDate);
                setError(businessPartner, acctServer, language);
                return false;
              }
            }

            // Checking the Tax Rate Valid To field
            final TaxRate taxRate = dao.getTaxRate(docLine.getM_C_Tax_ID());
            if (taxRate != null) {
              log4j.debug("Tax: " + taxRate.getIdentifier());
              if (!ValidToUtility.hasValidDateTo(taxRate, acctDate)) {
                log4j.error("The Tax Rate " + taxRate.getIdentifier()
                    + " has a valid to date less than " + acctDate);
                setError(taxRate, acctServer, language);
                return false;
              }
            }
          }
        }
      }
    }

    log4j.debug(documentNo + " (" + docType + ") OK");
    return true;
  }

  /**
   * Set the accounting error to be shown to the user in the given language. The error depends on
   * the baseOBObject class. Currently the following classes are supported:
   * <ul>
   * <li>Business Partner</li>
   * <li>Tax Rate</li>
   * <li>Element Value (Account)</li>
   * </ul>
   * 
   * @param baseOBObject
   * @param acctServer
   * @param language
   */
  private void setError(final BaseOBObject baseOBObject, final AcctServer acctServer,
      final String language) {
    final OBError error = new OBError();
    error.setType("Error");

    final String title = String.format(Utility.messageBD(new DalConnectionProvider(),
        "VALIDTO_ERROR_NOT_POSTED", language), baseOBObject.getIdentifier());
    error.setTitle(title);

    final String message;
    if (baseOBObject instanceof BusinessPartner) {
      message = "VALIDTO_BP";
    } else if (baseOBObject instanceof TaxRate) {
      message = "VALIDTO_TAXRATE";
    } else if (baseOBObject instanceof ElementValue) {
      message = "VALIDTO_ACCOUNT";
    } else {
      throw new InvalidParameterException("Instances of " + baseOBObject.getClass()
          + "class are not allowed");
    }

    error.setMessage(String.format(Utility
        .messageBD(new DalConnectionProvider(), message, language), baseOBObject.getIdentifier()));
    acctServer.setMessageResult(error);
  }
}
