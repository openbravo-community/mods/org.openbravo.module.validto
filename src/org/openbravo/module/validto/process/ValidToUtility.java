/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2010-2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.validto.process;

import java.lang.reflect.Method;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.structure.BaseOBObject;

public class ValidToUtility {
  static Logger log4j = Logger.getLogger(ValidToUtility.class);

  static boolean hasValidDateTo(final BaseOBObject baseOBObject, final Date dateAcct) {
    final String METHOD = "getValidToValidTo";
    return hasValidDateTo(baseOBObject, METHOD, dateAcct);
  }

  /**
   * Verifies the baseOBObject has a <i>valid to date</i> >= date. The <i>valid to date</i> field
   * can be defined with a different name for each BaseOBObject, so the methodName parameter
   * represents the baseOBObject method to determine the valid to value.<br />
   * Note: This method uses reflection for invoking the methodName passed as parameter
   * 
   * @param baseOBObject
   *          an Openbravo BaseOBObject with a <i>valid to</i> attribute
   * @param methodName
   *          the name of the method that gets the value of the <i>valid to</i> attribute. This
   *          method must accept 0 parameters and must return a java.util.Date object
   * @param date
   *          Date to compare to
   * @return true in case the <i>valid to</i> >= date
   */
  public static boolean hasValidDateTo(final BaseOBObject baseOBObject, final String methodName,
      final Date date) {
    if (baseOBObject == null)
      return true;

    final Class<? extends BaseOBObject> clazz = baseOBObject.getClass();
    final Method method;
    try {
      method = clazz.getMethod(methodName);
    } catch (final Exception e) {
      log4j.error("Error getting method " + methodName, e);
      throw new OBException("Error getting method " + methodName, e);
    }

    final Date validTo;
    try {
      validTo = (Date) method.invoke(baseOBObject);
    } catch (final Exception e) {
      log4j.error("Error invoking method " + methodName + " using the method: " + method.getName(), e);
      throw new OBException(e);
    }

    if (validTo == null)
      return true;

    if (validTo.compareTo(date) < 0)
      return false;
    else
      return true;
  }
}
