/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2010 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.validto.process;

import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;
import org.openbravo.model.financialmgmt.tax.TaxRate;

/**
 * This class contains the specific code used by the ValidToProcess class to access the database
 * using DAL (Hibernate)
 * 
 * @author openbravo
 * 
 */
public class ValidToProcessDao {

  /**
   * Gets the Tax Rate by the tax rate id
   * 
   * @param taxRateId
   * @return
   */
  public TaxRate getTaxRate(final String taxRateId) {
    return (TaxRate) getBaseOBObject(TaxRate.class, taxRateId);
  }

  /**
   * Gets the Business Partner by the business partner id
   * 
   * @param businessPartnerId
   * @return
   */
  public BusinessPartner getBusinessPartner(final String businessPartnerId) {
    return (BusinessPartner) getBaseOBObject(BusinessPartner.class, businessPartnerId);
  }

  /**
   * Gets the Element Value by the element value id
   * 
   * @param elementValueId
   * @return
   */
  public ElementValue getElementValue(final String elementValueId) {
    return (ElementValue) getBaseOBObject(ElementValue.class, elementValueId);
  }

  private <T extends BaseOBObject> T getBaseOBObject(final Class<T> clazz, final String id) {
    if (id == null || "".equals(id))
      return null;
    try {
      OBContext.setAdminMode(true);
      return OBDal.getInstance().get(clazz, id);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

}
